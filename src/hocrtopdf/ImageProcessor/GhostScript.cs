using System;
using System.IO;
using Clock.Utils;

namespace Clock.ImageProcessing
{
    public class GhostScript : OsUtil
    {
        public bool GhostScriptExists()
        {
            return File.Exists(GetExecutablePath());
        }

        private string GetExecutablePath()
        {
            return GetProgramPath("gs\\bin", "gswin64c.exe") ?? GetProgramPath("gs\\bin", "gswin32c.exe");
        }

        void RunCommand(string command)
        {
            ProcessCommand(GetExecutablePath(), command);
        }

        private string getOutPutFileName(string extWithDot)
        {
            if (IsLinux)
                return TempData.Instance.TempFileName(extWithDot);

            return "\"" + TempData.Instance.TempFileName(extWithDot) + "\"";

        }

        //gs -dSAFER -sDEVICE=png16m -dINTERPOLATE -dNumRenderingThreads=8
        //-dFirstPage=1 -dLastPage=1 -r300 -o ./output\_image.png -c 30000000 setvmthreshold
        //setvmthreshold -f my\_pdf.pdf

        public string ConvertPDFToPNG(string PDF, int StartPageNum, int EndPageNum)
        {
            string OutPut = getOutPutFileName(".png");
            PDF = "\"" + PDF + "\"";
            string command = String.Concat("-dNOPAUSE -r300 -q -dSAFER -sDEVICE=png16m -dINTERPOLATE -dNumRenderingThreads=8  -dBATCH -dFirstPage=", StartPageNum.ToString(), " -dLastPage=", EndPageNum.ToString(), " -sOutputFile=" + OutPut + " " + PDF + " 30000000 setvmthreshold -c quit");
            RunCommand(command);

            return new FileInfo(OutPut.Replace('"', ' ').Trim()).FullName;
        }

        public string ConvertPDFToBitmap(string PDF, int StartPageNum, int EndPageNum)
        {
            string OutPut = getOutPutFileName(".bmp");
            PDF = "\"" + PDF + "\"";
            string command = String.Concat("-dNOPAUSE -q -r300 -sDEVICE=bmp256 -dBATCH -dFirstPage=", StartPageNum.ToString(), " -dLastPage=", EndPageNum.ToString(), " -sOutputFile=" + OutPut + " " + PDF + " -c quit");
            RunCommand(command);

            return new FileInfo(OutPut.Replace('"', ' ').Trim()).FullName;
        }

        public string ConvertPDFToJPEG(string PDF, int StartPageNum, int EndPageNum)
        {
            string OutPut = getOutPutFileName(".jpeg");
            PDF = "\"" + PDF + "\"";
            string command = String.Concat("-dNOPAUSE -q -r300 -sDEVICE=jpeg -dNumRenderingThreads=8 -dBATCH -dFirstPage=", StartPageNum.ToString(), " -dLastPage=", EndPageNum.ToString(), " -sOutputFile=" + OutPut + " " + PDF + " -c quit");
            RunCommand(command);

            return new FileInfo(OutPut.Replace('"', ' ').Trim()).FullName;
        }

        public const string tiff12nc = "tiff12nc";
        public const string tiffg4 = "tiffg4";
        /// <summary>
        /// Converts entire PDF to a multipage-tiff
        /// </summary>
        /// <param name="PDF"></param>
        /// <returns></returns>
		public string ConvertPDFToMultiPageTiff(string PDF, string type)
        {
            string OutPut = TempData.Instance.TempFileName(".tif");

            PDF = "\"" + PDF + "\"";
            string command = "-dNOPAUSE -q -r300 -sDEVICE=" + type.ToString() + " -dBATCH -sOutputFile=" + OutPut + " " + PDF + " -c quit";
            RunCommand(command);

            return OutPut;
        }
    }
}

