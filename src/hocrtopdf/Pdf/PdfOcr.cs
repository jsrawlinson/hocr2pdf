using System;
using System.IO;
using Clock.Hocr;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Clock.Pdf
{
    public class SFont
    {
        private readonly Font _font;

        public SFont()
        {
            _font = new Font(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, false));
        }

        public SFont(string fontName)
        {
            var fontPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), fontName);
            _font = new Font(BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
        }

        public float BaseWidthPoint(string text, float size)
        {
            return Base.GetWidthPoint(text, size);
        }

        public BaseFont Base { get { return _font.BaseFont; } }

        public Font Font
        {
            get { return _font; }
        }
    }

    public class HOcrWriter
    {
        protected readonly SFont _font = new SFont();
        public void WriteLine(int pageNumber, hLine line, PdfStamper pdfStamper, PdfImportedPage page, int dpi)
        {
            line.AlignTops();

            foreach (hWord c in line.Words)
            {
                c.CleanText();

                BBox b = BBox.ConvertBBoxToPoints(c.BBox, dpi);

                if (b.Height > 50)
                {
                    continue;
                }

                PdfContentByte cb = pdfStamper.GetUnderContent(pageNumber);

                cb.BeginText();

                float size = 0; // Math.Round(b.Height);
                while (size < 50)
                {
                    var width = _font.BaseWidthPoint(c.Text, size);
                    if (width < b.Width)
                    {
                        size += 1;
                    }
                    else
                        break;
                }
                if (size < 10)
                {
                    size = size - 1;
                }

                if (size <= 0)
                {
                    size = 1;
                }

                cb.SetFontAndSize(_font.Base, b.Height >= 2 ? (int)size : 2);
                cb.SetTextMatrix(b.Left, page.Height - b.Top - b.Height);
                cb.SetWordSpacing(PdfWriter.SPACE);

                cb.ShowText(c.Text + " ");
                cb.EndText();
            }
        }
    }

    class LineHOcrWriter : HOcrWriter
    {
        public new void WriteLine(int pageNumber, hLine line, PdfStamper pdfStamper, PdfImportedPage page, int dpi)
        {
            line.CleanText();
            BBox b = BBox.ConvertBBoxToPoints(line.BBox, dpi);
            line.AlignTops();
            if (b.Height > 28)
                return;

            BBox lineBox = BBox.ConvertBBoxToPoints(line.BBox, dpi);
            PdfContentByte cb = cb = pdfStamper.GetUnderContent(pageNumber);


            cb.BeginText();

            float size = 0;
            while (1 == 1 && size < 50)
            {
                var width = _font.BaseWidthPoint(line.Text, size);
                if (width < b.Width)
                {
                    size += 1;
                }
                else
                    break;
            }

            cb.SetFontAndSize(_font.Base, b.Height >= 2 ? (int)size : 2);

            cb.SetTextMatrix(b.Left, page.Height - b.Top - b.Height + 2);
            cb.SetWordSpacing(.25f);
            cb.ShowText(line.Text);
            cb.EndText();
        }
    }
}