using System.Collections.Generic;
using Clock.Hocr;
using iTextSharp.text;
using iTextSharp.text.pdf.parser;

namespace Clock.Pdf
{
    internal class PdfTextExtractionStrategy : LocationTextExtractionStrategy
    {
        public IList<HOcrClass> Elements { get; private set; }
        public PdfTextExtractionStrategy()
        {
            Elements = new List<HOcrClass>();
        }
        void BeginTextBlock() { }
        void EndTextBlock() { }
        void RenderImage(ImageRenderInfo info) { }
        int id = 0;
        public override void RenderText(TextRenderInfo info)
        {

            Vector bottomLeft = info.GetDescentLine().GetStartPoint();
            Vector topRight = info.GetAscentLine().GetEndPoint();
            Rectangle rect = new Rectangle(bottomLeft[Vector.I1],
                bottomLeft[Vector.I2],
                topRight[Vector.I1],
                topRight[Vector.I2]);

            id += 1;
            BBox b = new BBox();
            b.Format = UnitFormat.Point;
            b.Height = rect.Height;
            b.Left = rect.Left;
            b.Height = rect.Height;
            b.Width = rect.Width;
            b.Top = rect.Top;
            var o = new HOcrClass();
            o.BBox = b;
            o.Text = info.GetText();
            o.ClassName = "xocr_word";
            o.Id = id.ToString();
            Elements.Add(o);
            base.RenderText(info);
        }

    }
}