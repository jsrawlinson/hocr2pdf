using System.IO;
using Clock.Hocr;
using iTextSharp.text.pdf;

namespace Clock.Pdf
{
    public class HOcrStamper : IHOcrStamper
    {
        public void Stamp(Stream input, Stream output, int pageNumber, hPage hpage)
        {
            using (var reader = new PdfReader(input))
            {
                using (var stamper = new PdfStamper(reader, output))
                {
                    var page = stamper.GetImportedPage(reader, pageNumber);
                    var writer = new HOcrWriter();
                    foreach (var para in hpage.Paragraphs)
                    {
                        foreach (var line in para.Lines)
                        {
                            writer.WriteLine(pageNumber, line, stamper, page, 300);
                        }
                    }
                }

                reader.RemoveUnusedObjects();
            }
        }
    }
}