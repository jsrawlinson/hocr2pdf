using System.IO;
using Clock.Hocr;
using Clock.Utils;

public class TesseractOcr : IOcr
{
    readonly string _language;
    readonly int Dpi = 300;

    public TesseractOcr(string language = "eng")
    {
        _language = language;
    }

    /// <summary>
    /// Ocr the specified page
    /// </summary>
    public hDocument GetHOcr(FileInfo image)
    {
        var hocr = OcrController.CreateHOCR(OcrMode.Tesseract, _language, image.FullName);
        var d = new hDocument();
        d.AddFile(hocr);
        TempData.Instance.Cleanup(hocr);
        return d;
    }
}