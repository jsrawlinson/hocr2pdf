﻿using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using Clock.Hocr;
using Clock.ImageProcessing;
using Clock.Utils;
using iTextSharp.text.pdf;

namespace Clock.Pdf
{ 
    public class PdfDoc : IDisposable
    {
        readonly GhostScript gs;
        readonly IOcr _ocr;
        readonly string _original;
        readonly string _copy;
        readonly string _modified;
        readonly Lazy<PdfReader> _lazyReader;

        protected PdfDoc()
        {
            gs = new GhostScript();
            _ocr = new TesseractOcr();
        }

        protected PdfDoc(string path) : this()
        {
            _original = path;
            _copy = TempData.Instance.TempFileName(".pdf");
            File.Copy(_original, _copy);
            _lazyReader = new Lazy<PdfReader>(() => new PdfReader(_copy));
        }

        #region publics

        public int Pages => Reader().NumberOfPages;

        public static PdfDoc Open(string path)
        {
            if (File.Exists(path))
            {
                return new PdfDoc(path);
            }

            throw new FileNotFoundException(path);
        }

        public void Save()
        {
            File.OpenWrite(_original).Close();
            File.Copy(_working, _original, true);
        }

        public void Save(string path, string password = null)
        {
            File.OpenWrite(path).Close();
            if (File.Exists(_modified))
            {
                File.Copy(_modified, path);
            }
        }

        public void Close()
        {
            File.Delete(_copy);
            File.Delete(_modified);
            _lazyReader.Value.Dispose();
        }

        /// <summary>
        /// Ocr the PDF
        /// </summary>
        public void Ocr()
        {
            var range = Enumerable.Range(1, Reader().NumberOfPages).ToArray();
            var list = range.Select(t => _ocr.GetHOcr(GetPageImage(t))).ToList();
            var working = TempData.Instance.TempFileName(".pdf");
            var stamper = new PdfStamper(Reader(), File.Create(_modified));
            
            using (stamper)
            {
                foreach (var i in range)
                {
                    var page = stamper.GetImportedPage(Reader(), i);
                    var writer = new HOcrWriter();
                    var hdoc = list.ElementAtOrDefault(i - 1);

                    var hpage = hdoc?.Pages.First();
                    foreach (var para in hpage?.Paragraphs ?? Enumerable.Empty<hParagraph>())
                    {
                        foreach (var line in para.Lines)
                        {
                            writer.WriteLine(i, line, stamper, page, 300);
                        }
                    }
                }
                stamper.Close();
            }
        }

        public FileInfo GetPageImage(int page)
        {
            var offset = Math.Min(1, page);
            if (gs.GhostScriptExists())
                return new FileInfo(gs.ConvertPDFToBitmap(_copy, offset, offset));

            throw new NotImplementedException("install goatsecript");
        }

        public void Dispose()
        {
            if (_lazyReader.IsValueCreated)
            {
                _lazyReader.Value.Dispose();
            }
        }

        #endregion

        #region privates
        private PdfReader Reader()
        {
            return _lazyReader.Value;
        }

        #endregion
    }
}
