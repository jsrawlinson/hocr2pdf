using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;

namespace Clock.Pdf
{
    internal class ParsedPageImage
    {
        public System.Drawing.Image Image { get; set; }
        public PdfImageObject PdfImageObject { get; set; }
        public int IndirectReferenceNum { get; set; }
        public PRStream PRStream { get; set; }
    }
}