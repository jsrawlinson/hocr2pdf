using System.Collections.Generic;
using System.Drawing;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;

namespace Clock.Pdf
{
    internal class MyImageRenderListener : IRenderListener
    {
        public void RenderText(TextRenderInfo renderInfo) { }
        public void BeginTextBlock() { }
        public void EndTextBlock() { }

        public PdfDoc Reader { get; set; }
        public int PageNumber { get; set; }
        public List<ParsedPageImage> ParsedImages = new List<ParsedPageImage>();
        PdfName filter;

        public void RenderImage(ImageRenderInfo renderInfo)
        {
            PdfImageObject image = null;

            try
            {
                image = renderInfo.GetImage();
                if (image == null) return;

                if (renderInfo.GetRef() == null)
                    return;

                int num = renderInfo.GetRef().Number;


                byte[] bytes = image.GetImageAsBytes();
                if (bytes == null)
                    return;

                ParsedPageImage pi = new ParsedPageImage();
                pi.IndirectReferenceNum = num;
                pi.PdfImageObject = image;


                filter = (PdfName)image.Get(PdfName.FILTER);
                pi.Image = image.GetDrawingImage();
                //                if (filter.ToString() == "/DCTDecode")
                //                {
                //                    byte[] dbytes = Reader.ExtractPagesToPDFBytes(string.Concat(PageNumber, "-", PageNumber), "");
                //                    byte[] dimgBytes = MuPdfConverter.ConvertPdfToTiff(dbytes, 300, RenderType.RGB, false, false, 0, "");
                //                    System.Drawing.Image dcolor = System.Drawing.Image.FromStream(new MemoryStream(dimgBytes));
                //                    pi.Image = dcolor;
                //                    ParsedImages.Add(pi);
                //                    return;
                //                }
                //
                //                if (filter.ToString() == "/JBIG2Decode")
                //                {
                //                    byte[] imgBytes;
                //                    bytes = Reader.ExtractPagesToPDFBytes(string.Concat(PageNumber, "-", PageNumber), "");
                //                    imgBytes = MuPdfConverter.ConvertPdfToTiff(bytes, 300, RenderType.Grayscale, false, false, 0, "");
                //                    System.Drawing.Image grey = System.Drawing.Image.FromStream(new MemoryStream(imgBytes));
                //                    grey = ImageProcessor.ConvertToBitonal(ImageProcessor.GetAsBitmap(grey));
                //                    pi.Image = grey;
                //                    ParsedImages.Add(pi);
                //                    return;
                //                }

                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    pi.Image = Image.FromStream(new MemoryStream(bytes));
                    ParsedImages.Add(pi);
                    return;

                }
            }
            catch (IOException ie)
            {
                //PdfIndirectReference r = renderInfo.GetRef();


                //bytes = Reader.ExtractPagesToPDFBytes(string.Concat(PageNumber, "-", PageNumber), "");
                //imgBytes = MuPdfConverter.ConvertPdfToTiff(bytes, 300, RenderType.RGB, false,false, 0, "");
                //System.Drawing.Image color = System.Drawing.Image.FromStream(new MemoryStream(imgBytes));

                //Images.Add(color);
            }

        }
    }
}