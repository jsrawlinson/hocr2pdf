namespace Clock.Pdf
{
    public enum Rotation { None = 0, Degrees90 = 90, Degrees180 = 180, Degrees270 = 270 }
}