﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clock.Pdf
{
    public class Bookmark
    {
        public Bookmark()
        {
            Bookmarks = new List<Bookmark>();
        }
        public string Id { get; set; }
        public string Title { get; set; }
        public int StartPage { get; set; }
        public int EndPage { get; set; }
        public IList<Bookmark> Bookmarks { get; set; }
    }
}
