using System.IO;
using Clock.Hocr;

public interface IOcr
{
    hDocument GetHOcr(FileInfo file);
}

public interface IHOcrStamper
{
    void Stamp(Stream input, Stream output, int pageNumber, hPage hpage);
}