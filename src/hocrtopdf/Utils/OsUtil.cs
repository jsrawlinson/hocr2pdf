using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Clock.Utils
{
    public abstract class OsUtil
	{
		internal static bool IsLinux
		{
			get
			{
				int p = (int) Environment.OSVersion.Platform;
				return (p == 4) || (p == 6) || (p == 128);
			}
		}

        public static void ProcessCommand(string FileToExecute, string Arguments)
        {
            Process p = new Process();
            ProcessStartInfo info = new ProcessStartInfo();
            info.FileName = FileToExecute;
            info.UseShellExecute = false;
            info.RedirectStandardError = true;
            info.RedirectStandardOutput = true;
            info.WindowStyle = ProcessWindowStyle.Hidden;
            info.CreateNoWindow = true;
            //-t .75
            info.Arguments = Arguments;
            p.StartInfo = info;
            try
            {
                p.Start();
                p.WaitForExit();
            }
            catch (Exception x)
            {
                Debug.WriteLine(x.Message);
                throw x;
            }
        }

        public static string EnvPath()
        {
            return Environment.GetEnvironmentVariable("PATH");
        }

        public static string GetProgramPath(string ProgramDirectoryName, string ExeName)
        {
            if (ProgramDirectoryName == null) throw new ArgumentNullException(nameof(ProgramDirectoryName));
            if (ExeName == null) throw new ArgumentNullException(nameof(ExeName));

            string pgFolder = System.IO.Path.Combine(Environment.GetEnvironmentVariable("ProgramFiles"), ProgramDirectoryName, ExeName);
            string pg86Folder = System.IO.Path.Combine(Environment.GetEnvironmentVariable("ProgramFiles(x86)"), ProgramDirectoryName, ExeName);

            var path = EnvPath().Split(';').Select(x => System.IO.Path.Combine(x, ExeName))
                .Concat(new[] { pgFolder, pg86Folder })
                .FirstOrDefault(File.Exists);

            if (!string.IsNullOrEmpty(path))
            {
                return path;
            }

            //Finally check directory of executing assembly
            string[] files = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, ExeName, SearchOption.AllDirectories);
            return files.FirstOrDefault();
        }
	}
}

