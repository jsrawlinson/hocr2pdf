﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clock.Hocr;
using Clock.Pdf;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var pdf = PdfDoc.Open("D:\\temp\\hOcr2Pdf\\ps\\original.pdf"))
            {
                pdf.Ocr();
                pdf.Save("D:\\temp\\hOcr2Pdf\\ps\\modified.pdf");
            }

            //var hDoc = new hDocument();
            //hDoc.AddFile("D:\\temp\\hOcr2Pdf\\ps\\test.html");
        }
    }
}
