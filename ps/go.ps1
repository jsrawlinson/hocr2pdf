﻿<#
    Scratch Pad for hOcr2Pdf.net
#>

Set-ExecutionPolicy RemoteSigned
Add-Type -Path "itextsharp.dll"
Add-Type -Path "HtmlAgilityPack.dll"
Add-Type -Path "hOcr2Pdf.dll"


Try
{
    $pdf = New-Object Clock.Pdf.PdfCreator -ArgumentList "D:\\Temp\\hOcr2Pdf\\ballsysssss.pdf"
    $pdf.AddHocrFile(".\test.html")    
}
Catch 
{
    Write-Output $_
}
Finally
{
    $pdf.Dispose()
}